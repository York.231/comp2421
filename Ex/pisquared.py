import numpy as np
import sys

def pisquared(n):

    pi2 = 0.
    for k in range(1,n+1):
        #print(k)
        pi2 = pi2 + 1./(k*k)

    return 6. * pi2

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Please enter a value')
    else:
        print(pisquared(int(sys.argv[1])))
