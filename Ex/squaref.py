import sys

def findSquare(nsq):
    i = 1
    while i*i < nsq:
        i += 1

    return i


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Please enter a number!')
    else:
        print(findSquare(int(sys.argv[1])))
